//Import thu vien express
const express = require("express");
//Khai bao middleware
const drinkMiddleware = require("../middleware/drinkMiddleware");
//import controller
const { getAllDrink, getDrinkById, createDrink, updateDrink, deleteDrink } = require("../controller/drinkController");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL drink: ", req.url);

    next();
});
//get all drink
router.get("/drinks", drinkMiddleware.getAllDrinkMiddleware, getAllDrink)
//create drink
router.post("/drinks", drinkMiddleware.postDrinkMiddleware, createDrink);
//get a drink
router.get("/drinks/:drinkId", drinkMiddleware.getADrinkMiddleware, getDrinkById);
//update drink
router.put("/drinks/:drinkId", drinkMiddleware.putDrinkMiddleware, updateDrink);
//delete drink
router.delete("/drinks/:drinkId", drinkMiddleware.deleteDrinkMiddleware, deleteDrink);

module.exports = router;