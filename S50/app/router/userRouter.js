//Import thu vien express
const express = require("express");
//Khai bao middleware
const userMiddleware = require("../middleware/userMiddleware");
//import controller
const { getAllUser, createUser, getUserById, updateUser, deleteUser } = require("../controller/userController");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL user: ", req.url);

    next();
});
//get all user
router.get("/users", userMiddleware.getAllUsersMiddleware,getAllUser )
//create user
router.post("/users", userMiddleware.postUsersMiddleware,createUser );
//get a user
router.get("/users/:userId", userMiddleware.getAUsersMiddleware,getUserById );
//update user
router.put("/users/:userId", userMiddleware.putUsersMiddleware,updateUser );
//delete user
router.delete("/users/:userId", userMiddleware.deleteUsersMiddleware,deleteUser );

module.exports = router;