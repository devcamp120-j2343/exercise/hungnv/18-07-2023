//Import thu vien express
const express = require("express");
//Khai bao middleware
const orderMiddleware = require("../middleware/orderMiddleware");
//import controller
const { getAllOrders, createOrder, getOrderById, updateOrder, deleteOrder } = require("../controller/orderControler");
//Tao ra router
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL order: ", req.url);

    next();
});
//get all order
router.get("/orders", orderMiddleware.getAllOrdersMiddleware,getAllOrders )
//create order
router.post("/users/:userId/orders", orderMiddleware.postOrdersMiddleware,createOrder );
//get a order
router.get("/orders/:orderId", orderMiddleware.getAOrdersMiddleware,getOrderById );
//update order
router.put("/orders/:orderId", orderMiddleware.putOrdersMiddleware,updateOrder );
//delete order
router.delete("/orders/:orderId", orderMiddleware.deleteOrdersMiddleware,deleteOrder );

module.exports = router;