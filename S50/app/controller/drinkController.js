//Import course model
const { default: mongoose } = require('mongoose');
const drinkModel = require('../models/drinkModel');

//Tao cac function crud
//get all course
const getAllDrink = (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    drinkModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(200).json({
                    status: "Get all drink sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
const getDrinkById = (req, res) => {
    //B1: thu thập dữ liệu
    var drinkId = req.params.drinkId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    //B3: thực thi model
    drinkModel.findById(drinkId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Get drink by id sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any drink",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//create a drink
const createDrink = (req, res) => {
    //B1: thu thập dữ liệu
    const { maNuocUong, tenNuocUong, donGia } = req.body;

    //B2: kiểm tra dữ liệu
    if (!maNuocUong) {
        return res.status(400).json({
            status: "Bad request",
            message: "ma nuoc uong is required!"
        })
    }
    if (!tenNuocUong) {
        return res.status(400).json({
            status: "Bad request",
            message: "ten nuoc uong is required!"
        })
    }
    if (!Number.isInteger(donGia) || donGia < 0) {
        return res.status(400).json({
            status: "Bad request",
            message: "donGia is invalid!"
        })
    }

    //B3: thực hiện thao tác model
    let newDrink = {
        _id: new mongoose.Types.ObjectId(),
        maNuocUong,
        tenNuocUong,
        donGia
    }

    drinkModel.create(newDrink)
        .then((data) => {
            return res.status(201).json({
                status: "Create new drink sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//update drink
const updateDrink = (req, res) => {
    //B1: thu thập dữ liệu
    var drinkId = req.params.drinkId;
    const { maNuocUong, tenNuocUong, donGia } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if (!maNuocUong) {
        return res.status(400).json({
            status: "Bad request",
            message: "ma nuoc uong is required!"
        })
    }
    if (!tenNuocUong) {
        return res.status(400).json({
            status: "Bad request",
            message: "ten nuoc uong is required!"
        })
    }
    if (!Number.isInteger(donGia) || donGia < 0) {
        return res.status(400).json({
            status: "Bad request",
            message: "donGia is invalid!"
        })
    }

    //B3: thực thi model
    let updateDrink = {
        maNuocUong,
        maNuocUong,
        donGia
    }

    drinkModel.findByIdAndUpdate(drinkId, updateDrink)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Update drink sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}
//delete drink
const deleteDrink = (req, res) => {
    //B1: Thu thập dữ liệu
    var drinkId = req.params.drinkId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    drinkModel.findByIdAndDelete(drinkId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Delete drink sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any drink",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

//Export thanh module
module.exports = {
    getAllDrink,
    getDrinkById,
    createDrink,
    updateDrink,
    deleteDrink
}