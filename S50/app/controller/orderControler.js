//import thư viện mongoose
const mongoose = require('mongoose');

// import order model
const orderModel = require('../models/orderModel');

// import user model
const userModel = require('../models/userModel');
// const create order 
const createOrder = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        userId,
        orderCode,
        pizzaSize,
        pizzaType,
        status
    } = req.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "User ID is not valid"
        })
    }

    if (!orderCode) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "orderCode is required"
        })
    }
    if (!pizzaSize) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "pizzaSize is required"
        })
    }
    if (!pizzaType) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "pizzaType is required"
        })
    }
    if (!status) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "status is required"
        })
    }


    // B3: Thao tác với CSDL
    var newOrder = {
        _id: new mongoose.Types.ObjectId(),
        orderCode: orderCode,
        pizzaSize: pizzaSize,
        pizzaType: pizzaType,
        status: status
    }

    try {
        const createdOrder = await orderModel.create(newOrder);

        const updatedUser = await userModel.findByIdAndUpdate(userId, {
            $push: { orders: createdOrder._id }
        });

        return res.status(201).json({
            status: "Create order successfully",
            user: updatedUser,
            data: createOrder
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: err.message
        })
    }
}
//get all orders
const getAllOrders = async (req, res) => {
    //B1: thu thập dữ liệu
    const userId = req.query.userId;
    //B2: kiểm tra
    if (userId !== undefined && !mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "User ID is not valid"
        })
    }

    //B3: thực thi model
    try {
        if (userId === undefined) {
            const orderList = await orderModel.find();

            if (orderList && orderList.length > 0) {
                return res.status(200).json({
                    status: "Get all orders sucessfully",
                    data: orderList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any order"
                })
            }
        } else {
            const userInfo = await userModel.findById(userId).populate("users");

            return res.status(200).json({
                status: "Get all orders of users  sucessfully",
                data: userInfo.orders
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
//get a order
const getOrderById = async (req, res) => {
    //B1: thu thập dữ liệu
    var orderId = req.params.orderId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    //B3: thực thi model
    try {
        const orderInfo = await reviewModel.findById(orderId);

        if (orderInfo) {
            return res.status(200).json({
                status: "Get order by id sucessfully",
                data: orderInfo
            })
        } else {
            return res.status(404).json({
                status: "Not found any order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
//update order
const updateOrder = async (req, res) => {
    //B1: thu thập dữ liệu
    var orderId = req.params.orderId;

    const { orderCode, pizzaSize, pizzaType, status } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if (!orderCode) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "orderCode is required"
        })
    }
    if (!pizzaSize) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "pizzaSize is required"
        })
    }
    if (!pizzaType) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "pizzaType is required"
        })
    }
    if (!status) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "status is required"
        })
    }

    //B3: thực thi model
    try {
        let updateOrder = {
            orderCode,
            pizzaSize,
            pizzaType,
            status
        }

        const updatedOrder = await reviewModel.findByIdAndUpdate(reviewId, updateOrder);

        if (updatedOrder) {
            return res.status(200).json({
                status: "Update order sucessfully",
                data: updatedReview
            })
        } else {
            return res.status(404).json({
                status: "Not found any order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
//delete order
const deleteOrder = async (req, res) => {
    //B1: Thu thập dữ liệu
    var orderId = req.params.orderId;
    // Nếu muốn xóa id thừa trong mảng order thì có thể truyền thêm userId để xóa
    var userId = req.query.userId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    try {
        const deletedOrder = await orderModel.findByIdAndDelete(orderId);

        // Nếu có courseId thì xóa thêm (optional)
        if (orderId !== undefined) {
            await userModel.findByIdAndUpdate(userId, {
                $pull: { orders: orderId }
            })
        }

        if (deletedOrder) {
            return res.status(200).json({
                status: "Delete order sucessfully",
                data: deletedOrder
            })
        } else {
            return res.status(404).json({
                status: "Not found any order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createOrder,
    getAllOrders,
    getOrderById,
    updateOrder,
    deleteOrder
}
