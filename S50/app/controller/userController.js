//Import course model
const { default: mongoose } = require('mongoose');
const userModel = require('../models/userModel');

//Tao cac function crud
//  create user 
const getAllUser = async (req, res) => {
   //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    userModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(200).json({
                    status: "Get all user sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//get a user
const getUserById = (req, res) => {
    //B1: thu thập dữ liệu
    var userId = req.params.userId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    //B3: thực thi model
    userModel.findById(userId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Get user by id sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any user",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//create a user
const createUser = (req, res) => {
    //B1: thu thập dữ liệu
    const { fullName, email, address,phone } = req.body;

    //B2: kiểm tra dữ liệu
    if (!fullName) {
        return res.status(400).json({
            status: "Bad request",
            message: "fullname is required!"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required!"
        })
    }
    if (!address) {
        return res.status(400).json({
            status: "Bad request",
            message: "address is required!"
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is required!"
        })
    }

    //B3: thực hiện thao tác model
    let newUser = {
        _id: new mongoose.Types.ObjectId(),
        fullName,
        email,
        address,
        phone
    }

    userModel.create(newUser)
        .then((data) => {
            return res.status(201).json({
                status: "Create new user sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//update user
const updateUser = (req, res) => {
    //B1: thu thập dữ liệu
    var userId = req.params.userId;
    const { fullName, email, address,phone } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if (!fullName) {
        return res.status(400).json({
            status: "Bad request",
            message: "fullname is required!"
        })
    }
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "email is required!"
        })
    }
    if (!address) {
        return res.status(400).json({
            status: "Bad request",
            message: "address is required!"
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is required!"
        })
    }

    //B3: thực thi model
    let updateUser = {
        fullName,
        email,
        address,
        phone
    }

    userModel.findByIdAndUpdate(userId, updateUser)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Update user sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any user",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}
//delete user
const deleteUser = (req, res) => {
    //B1: Thu thập dữ liệu
    var userId = req.params.userId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }

    userModel.findByIdAndDelete(userId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: "Delete user sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any user",
                    data
                })
            }

        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}
//Export thanh module
module.exports = {
    getAllUser,
    getUserById,
    createUser,
    updateUser,
    deleteUser
}