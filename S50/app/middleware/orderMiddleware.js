const getAllOrdersMiddleware = (req, res, next) => {
    console.log("Get All Orders!");
    next();
}

const getAOrdersMiddleware = (req, res, next) => {
    console.log("Get a Orders!");
    next();
}
const postOrdersMiddleware = (req, res, next) => {
    console.log("Create a Orders!");
    next();
}

const putOrdersMiddleware = (req, res, next) => {
    console.log("Update a Orders!");
    next();
}
const deleteOrdersMiddleware = (req, res, next) => {
    console.log("Delete a Orders!");
    next();
}

//export
module.exports = {
    getAllOrdersMiddleware,
    getAOrdersMiddleware,
    postOrdersMiddleware,
    putOrdersMiddleware,
    deleteOrdersMiddleware
}