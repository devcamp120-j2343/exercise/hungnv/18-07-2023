const getAllUsersMiddleware = (req, res, next) => {
    console.log("Get All Users!");
    next();
}

const getAUsersMiddleware = (req, res, next) => {
    console.log("Get a Users!");
    next();
}
const postUsersMiddleware = (req, res, next) => {
    console.log("Create a Users!");
    next();
}

const putUsersMiddleware = (req, res, next) => {
    console.log("Update a Users!");
    next();
}
const deleteUsersMiddleware = (req, res, next) => {
    console.log("Delete a Users!");
    next();
}

//export
module.exports = {
    getAllUsersMiddleware,
    getAUsersMiddleware,
    postUsersMiddleware,
    putUsersMiddleware,
    deleteUsersMiddleware
}