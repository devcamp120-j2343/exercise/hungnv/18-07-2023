const getAllDrinkMiddleware = (req, res, next) => {
    console.log("Get All Drink!");
    next();
}

const getADrinkMiddleware = (req, res, next) => {
    console.log("Get a Drink!");
    next();
}
const postDrinkMiddleware = (req, res, next) => {
    console.log("Create a Drink!");
    next();
}

const putDrinkMiddleware = (req, res, next) => {
    console.log("Update a Drink!");
    next();
}
const deleteDrinkMiddleware = (req, res, next) => {
    console.log("Delete a Drink!");
    next();
}

//export
module.exports = {
    getAllDrinkMiddleware,
    getADrinkMiddleware,
    postDrinkMiddleware,
    putDrinkMiddleware,
    deleteDrinkMiddleware
}